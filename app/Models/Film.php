<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'films';
    protected $fillable = ['title','img'];

    public function genres()
    {
        return $this->belongsToMany(Genre::class);
    }

    public static function updateData(int $id, mixed $title)
    {
        return Film::where('id', $id)
            ->update(['title' => $title]);
    }


    public static function changeStatus($id)
    {
        $status = Film::find($id);
        if ($status->status === 'inactive') {
            $status->status = 'active';
            $status->save();
        } else {
            $status->status = 'inactive';
            $status->save();
        }
    }
}
