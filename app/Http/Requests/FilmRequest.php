<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FilmRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => [
                'min:3',
                'max:200',
                'string',
            ],
            'img' => [
                'required',
                'image',
                'mimes:jpg,jpeg,png,gif,bmp,svg',
                'max:20480',
            ],
        ];
    }
}
