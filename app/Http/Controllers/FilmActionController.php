<?php

namespace App\Http\Controllers;

use App\Models\Film;
use App\Models\Genre;
use Illuminate\Http\Request;

class FilmActionController extends Controller
{
    public function index()
    {
        return view('film.index', [
            'films' => Film::where('status', 'active')->paginate(8),
            'genres' => Genre::all()
        ]);
    }
}
