<?php

namespace App\Http\Controllers;

use App\Models\Film;
use App\Models\FilmGenre;
use App\Models\Genre;
use Illuminate\Http\Request;
use App\Http\Requests\FilmRequest;

class FilmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('film.index', [
            'films' => Film::paginate(8),
            'genres' => Genre::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('film.create', [
            'genres' => Genre::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(FilmRequest $request)
    {
        $path = $request->file('img')->store('film_img', 'public');

        Film::create(
            [
                'title' => $request->title,
                'img' => $path
            ]
        );
        FilmGenre::create([
            'film_id' => Film::max('id'),
            'genre_id' => $request->genre_id
        ]);
        return redirect(route('film.index'))->with('message', 'Film "' . $request->title . '", added.');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        Film::changeStatus($id);
        return redirect(route('film.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('film.edit', [
            'film' => Film::find($id),
            'genres' => Genre::all(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(FilmRequest $request, $id)
    {
        Film::updateData($id, $request->title);
        return redirect(route('film.index'))->with('message', 'Film "' . $request->title . '", updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $title = Film::find($id)->title;
        $FilmGenreId = FilmGenre::where('film_id', $id)->first()->id;

        //удаление записи с таблици film_genre
        FilmGenre::destroy($FilmGenreId);

        // удаление записи с таблици films
        Film::destroy($id);

        return redirect(route('film.index'))->with('message', 'Film "' . $title . '", deleted.');
    }
}
