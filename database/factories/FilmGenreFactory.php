<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class FilmGenreFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'film_id' => $this->faker->numberBetween(1, 25),
            'genre_id' => $this->faker->numberBetween(1, 8)
        ];
    }
}
