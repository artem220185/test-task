<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
//         \App\Models\Film::factory(25)->create();
//         \App\Models\Genre::factory(8)->create();
         \App\Models\FilmGenre::factory(15)->create();
    }
}
