@extends('main')

@section('content')

    <div class="container pt-4 ">
        @if (session('message'))
            <div class="toast-body bg-secondary text-white">
                {{ session('message') }}
            </div>
        @endif
        <div class="row d-flex">
            <div class="col-3">
                @foreach($genres as $genre)
                    <p><a href="#">{{ $genre->title }}</a></p>
                @endforeach

                <a href="{{ route('published') }}"
                   class="btn btn-outline-warning ml-3 mb-2">only published films</a>
                <p>
                    <a href="{{ route('film.index') }}"
                       class="btn btn-outline-success ml-3 mb-2">main page</a>
                </p>
            </div>
            <div class="col">
                <div class="row">
                    @foreach($films as $film)
                        <div class="col-3">
                            <p>{{ $film->title }}</p>

                            <div class="img-fluid">
                                @if($film->img)
                                    <img class="img-fluid" src="{{ asset('/storage/'.$film->img)}}" alt="poster">
                                @else
                                    <img class="img-fluid" src="/storage/film_img/def.jpg"
                                         alt="poster">
                                @endif
                            </div>

                            {{-- активны/не активный --}}
                            <form method="get"
                                  action="{{ route('film.show',['film'=>$film->id]) }}"
                                  role="form"
                                  class="bd-highlight mb-2">
                                @csrf
                                @if( $film->status == 'active')
                                    <input class="mt-2 btn btn-block btn-outline-success btn-sm"
                                           type="submit"
                                           value="{{ $film->status }}">
                                @else
                                    <input class="mt-2 btn btn-block btn-outline-secondary btn-sm"
                                           type="submit"
                                           value="{{ $film->status }}">
                                @endif
                            </form>
                            {{--------------------}}

                            <a href="{{ route('film.edit', ['film'=>$film->id]) }}"
                               class="btn btn-outline-warning ml-3 mb-2">Edit</a>
                            <form
                                action="{{ route('film.destroy', ['film'=>$film->id]) }}"
                                method="post" class="mb-4">
                                @csrf
                                @method('delete')
                                <button type="submit" title="delete forever" type="button"
                                        class="btn btn-outline-danger ml-3">Delete
                                </button>
                            </form>
                        </div>
                    @endforeach
                </div>
                <a href="{{ route('film.create') }}" class="btn btn-primary ml-3 mb-4">Сreate a poster</a>

                {{ $films->links() }}
            </div>
        </div>
    </div>
@endsection
