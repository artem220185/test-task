@extends('main')
@section('content')

    <div class="py-5 text-center container">

        <h2 class="fw-light">Create new film</h2>

        <form enctype="multipart/form-data" method="post" action="{{ route('film.store') }}">
            @csrf
            <div class="card-body">
                {{--title--}}
                <div class="form-group text-start mb-4">
                    <label class="" for="exampleInputTitle">Title</label>
                    <input name="title" type="text" class="form-control @error('title') is-invalid @enderror"
                           value="{{ old('title') }}"
                           id="exampleInputEmail1" placeholder="Enter title">
                    @error('title')
                    <span class="alert-danger">{{ $message }}</span>
                    @enderror
                </div>

                {{--genre--}}
                <div class="form-group text-start mb-4">
                    <label for="exampleInputCategory">Genre</label>
                    <select name="genre_id" id="exampleInputCategory"
                            class="form-control @error('genre_id') is-invalid @enderror">
                        <option>Please select genre</option>

                        @foreach($genres as $genre)
                            <option @if(old('genre_id') && old('genre_id') == $genre->id)
                                    selected
                                    @endif
                                    value="{{ $genre->id }}">{{ $genre->title }}
                            </option>
                        @endforeach
                    </select>
                    @error('genre_id')
                    <span class="alert-danger">{{ $message }}</span>
                    @enderror
                </div>

                {{--imgage--}}
                <div class="form-group mt-4 mb-4 text-start  ">
                    <label for="exampleInputFile">Film image</label>
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" name="img"
                                   class="custom-file-input @error('img') is-invalid @enderror"
                                   id="exampleInputFile">
                        </div>
                    </div>
                </div>
                @error('img')
                <span class="alert-danger">{{ $message }}</span>
                @enderror
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Add</button>
            </div>
        </form>

    </div>

@endsection()
